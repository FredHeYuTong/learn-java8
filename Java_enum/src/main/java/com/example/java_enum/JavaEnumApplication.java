package com.example.java_enum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaEnumApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaEnumApplication.class, args);
    }

}
