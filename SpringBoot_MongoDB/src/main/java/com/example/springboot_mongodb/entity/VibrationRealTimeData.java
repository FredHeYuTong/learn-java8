package com.example.springboot_mongodb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * @projectName: learn-java
 * @packName: com.example.java_mongodb.entity
 * @className: VibrationRealTimeData
 * @author: FredHe
 * @date: 2022/11/18 11:13
 * @description: 振动设备实时监测数据
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "vibration_realtime_data") //指定文档名（表名）
public class VibrationRealTimeData {

    @Field(name = "tag_name")
    private String tagName;

    @Field(name = "real_time")
    private String realTime;

    @Field(name = "exp_time")
    @Indexed(name = "exp_time")
    private Date expTime;

}
