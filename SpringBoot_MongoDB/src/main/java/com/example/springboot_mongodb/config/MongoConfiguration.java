package com.example.springboot_mongodb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoDatabaseFactorySupport;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;

import javax.annotation.Resource;

/**
 * @projectName: learn-java
 * @packName: com.example.java_mongodb.config
 * @className: MongodbConfig
 * @author: FredHe
 * @date: 2022/11/18 10:09
 * @description: mongodb的配置类
 */
@Configuration
public class MongoConfiguration {

    @Resource
    private MongoDatabaseFactorySupport mongoDatabaseFactorySupport;
    @Resource
    private MappingMongoConverter mappingMongoConverter;

    @Bean
    public MongoTemplate mongoTemplate() {

        mappingMongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDatabaseFactorySupport, mappingMongoConverter);
        return mongoTemplate;
    }
}
