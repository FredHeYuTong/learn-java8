package com.example.springboot_mongodb;

import com.alibaba.fastjson.JSONObject;
import com.example.springboot_mongodb.entity.VibrationRealTimeData;
import com.mongodb.client.model.IndexOptions;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.util.BsonUtils;

import javax.annotation.Resource;
import javax.jws.Oneway;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class SpringBootMongoDbApplicationTests {

    //@Test
    void contextLoads() {
    }


    @Resource
    private MongoTemplate mongoTemplate;

    @Test
    void insertDataTest(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        VibrationRealTimeData vibrationRealTimeData = new VibrationRealTimeData();
        vibrationRealTimeData.setTagName("TG_TEST4");
        vibrationRealTimeData.setRealTime(simpleDateFormat.format(new Date()));
        vibrationRealTimeData.setExpTime(new Date());
        mongoTemplate.insert(vibrationRealTimeData,"vibration_realtime_data");
    }

    @Test
    void queryDataTest(){
        VibrationRealTimeData vibrationRealTimeData = new VibrationRealTimeData();
        vibrationRealTimeData.setTagName("TG_TEST4");
        Query query = new Query(Criteria.where("tag_name").is(vibrationRealTimeData.getTagName()));
        System.out.println(mongoTemplate.find(query, VibrationRealTimeData.class,"vibration_realtime_data"));
    }

    @Test
    void deleteDataTest(){
        VibrationRealTimeData vibrationRealTimeData = new VibrationRealTimeData();
        vibrationRealTimeData.setTagName("TG_TEST1");
        Query query = new Query(Criteria.where("tag_name").is(vibrationRealTimeData.getTagName()));
        // remove 方法会将全部相同地查询主键数据全部删除 所以请保证查询字段数据的唯一性 防止误删除
        mongoTemplate.remove(query,VibrationRealTimeData.class,"vibration_realtime_data");
    }

    /**
     * @methodName insertExpireDataTest
     * @description 设置过期时间
     * 注： 此过期时间设置是
     * new IndexOptions().expireAfter(10l,TimeUnit.SECONDS)
     * @author FredHe
     * @param
     * @return
     * @since 2022/11/24 20:22
     */
    @Test
    void insertExpireDataTest() {
        HashMap indexMap = new HashMap<String,Object>();
        indexMap.put("exp_time",1);
        Bson bson = BsonUtils.asBson(JSONObject.toJSON(indexMap));
        System.out.println(bson);
        // 创建TTL定时删除索引  索引是惟一的
        mongoTemplate.getCollection("vibration_realtime_data")
                .createIndex(bson,new IndexOptions().expireAfter(100l, TimeUnit.SECONDS));
        // 删除TTL定时删除索引
        /*mongoTemplate.getCollection("vibration_realtime_data")
                .dropIndex(bson);*/
    }
}
