package com.example.java8Demo.java8function;

import java.util.function.Function;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnFunction
 * @Author: FredHe
 * @Date: 2022/4/8 17:00
 * @Description: 学习Function接口
 */
public class DemoLearnFunction {

    public static void main(String[] args) {
        test(msg->{
            return Integer.parseInt(msg);
        });
    }

    public static void test(Function<String,Integer> function){
        Integer apply = function.apply("666");
        System.out.println("apply = "+ apply);
    }
}
