package com.example.java8Demo.java8function;

import java.util.Locale;
import java.util.function.Consumer;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnConsumer
 * @Author: FredHe
 * @Date: 2022/4/5 17:01
 * @Description: Consumer函数式接口学习
 */
public class DemoLearnConsumer {
    public static void main(String[] args) {
        test(msg ->
                //有参 无返回值 使用省略写法
            System.out.println(msg+"->转换为小写"+msg.toLowerCase(Locale.ROOT))
        );

    }
    private static void test(Consumer<String> consumer){
        consumer.accept("Hello World");
    }
}
