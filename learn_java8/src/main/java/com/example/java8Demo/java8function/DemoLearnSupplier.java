package com.example.java8Demo.java8function;

import java.util.Arrays;
import java.util.function.Supplier;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnSupplier
 * @Author: FredHe
 * @Date: 2022/4/5 16:51
 * @Description: Supplier函数式接口学习
 */
public class DemoLearnSupplier {
    public static void main(String[] args) {
        fun1(()->{
            int arr[] = {22,33,55,66,44,99,10};
            //计算数组的最大值
            Arrays.sort(arr);
            return arr[arr.length-1];
        });
    }

    private static void fun1(Supplier<Integer> supplier){
        //get() 是一个无参有返回值的 抽象方法
        Integer max = supplier.get();
        System.out.println("max="+max);

    }
}
