package main.java.com.example.java8Demo.java8function;

import java.util.Locale;
import java.util.function.Consumer;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnConsumer
 * @Author: FredHe
 * @Date: 2022/4/5 17:01
 * @Description: Consumer函数式接口学习
 */
public class DemoLearnConsumerAndThen {
    public static void main(String[] args) {
        test2(msg1->{
            System.out.println(msg1+"->转换为小写"+msg1.toLowerCase(Locale.ROOT));
        },msg2->{
            System.out.println(msg2+"->转换为大写"+msg2.toUpperCase(Locale.ROOT));
        });
    }

    private static void test2(Consumer<String> c1,Consumer<String> c2){
        String str = "Hello World";
        c1.andThen(c2).accept(str);
    }
}
