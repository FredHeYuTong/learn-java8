package main.java.com.example.java8Demo.java8function;

import java.util.function.Predicate;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnPredicate
 * @Author: FredHe
 * @Date: 2022/4/8 17:34
 * @Description: 函数式接口Predicate学习
 */
public class DemoLearnPredicateDefault {

    public static void main(String[] args) {

        test(msg1->{
            return msg1.contains("H");
        },msg2->{
            return msg2.contains("W");
        });
    }

    private static void test(Predicate<String> p1,Predicate<String> p2){
        boolean test = p1.test("Hello");
        boolean test2 = p2.test("World");
        //test 包含h test2 包含w
        System.out.println("test="+test);
        System.out.println("test2="+test2);
        //test 同时包含h test2 同时包含w
        boolean hello = p1.and(p2).test("Hello");
        //test 包含h 或者 test2 包含w
        boolean hello1 = p1.or(p2).test("Hello");
        //test 不包含h
        boolean hello2 = p1.negate().test("Hello");

        System.out.println(hello);  //false
        System.out.println(hello1); //true
        System.out.println(hello2); //false


    }
}
