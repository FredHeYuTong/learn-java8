package main.java.com.example.java8Demo.java8function;

import java.util.function.Function;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnFunctionAndThen
 * @Author: FredHe
 * @Date: 2022/4/8 17:00
 * @Description: 学习Function接口
 */
public class DemoLearnFunctionAndThen {

    public static void main(String[] args) {
        test(msg->{
            return Integer.parseInt(msg);
        },msg2->{
            return msg2*10;
        });
    }

    public static void test(Function<String,Integer> function,Function<Integer,Integer> function2){
        Integer apply = function.apply("666");
        Integer apply1 = function2.apply(apply);
        System.out.println("apply1 = "+ apply1);
    }
}
