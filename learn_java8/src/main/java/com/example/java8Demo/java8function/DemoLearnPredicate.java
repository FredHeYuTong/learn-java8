package com.example.java8Demo.java8function;

import java.util.function.Predicate;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: DemoLearnPredicate
 * @Author: FredHe
 * @Date: 2022/4/8 17:34
 * @Description: 函数式接口Predicate学习
 */
public class DemoLearnPredicate {

    public static void main(String[] args) {
        test(msg->{
            return msg != null;
        });

        test2(msg->{
            return msg.length()>100;
        },"chuidhiughiuh");
    }

    public static void test(Predicate<String> predicate){
        boolean hello = predicate.test("hello");
        System.out.println("hello="+hello);
    }

    private static void test2(Predicate<String> predicate,String msg){
        boolean test = predicate.test(msg);
        System.out.println("test="+test);
    }
}
