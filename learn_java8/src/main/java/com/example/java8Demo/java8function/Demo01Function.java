package com.example.java8Demo.java8function;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8function
 * @ClassName: Demo01Function
 * @Author: FredHe
 * @Date: 2022/4/5 11:16
 * @Description: 函数接口学习
 */
public class Demo01Function {

    public static void main(String[] args) {

        fun1(abc -> {
            int sum = 0;
            for (int i : abc) {
                sum += i;
            }
            return sum;
        });
    }

    public static void fun1(Operator operator){
        int[] arr = {1,2,3,4};
        int sum = operator.getSum(arr);
        System.out.println("sum="+sum);
    }


    /**
     * @MethodName
     * @Description 函数式接口
     * @Author FredHe
     * @Param
     * @Return {@link null}
     * @Since 2022/4/5 11:46
     */
    @FunctionalInterface
    interface Operator{
        /**
         * @MethodName getSum
         * @Description 求和接口
         * @Author FredHe
         * @Param
         * @Return {@link null}
         * @Since 2022/4/5 11:17
         */
        int getSum(int[] arr);
    }
}
