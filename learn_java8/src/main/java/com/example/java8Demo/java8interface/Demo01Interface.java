package com.example.java8Demo.java8interface;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8interface
 * @ClassName: Demo01Inerface
 * @Author: FredHe
 * @Date: 2022/4/5 9:38
 * @Description: java8接口部分学习
 */
public class Demo01Interface {
    public static void main(String[] args) {
        A b = new B();
        b.test3();
        A c = new C();
        c.test3();

        A.test4();
    }


    interface A{
         void test1();

         //在接口中新增抽象方法 所有实现类都需要重写这个方法 不利于接口的扩展
        void test2();

        /**
         * @MethodName test3
         * @Description 接口中定义的默认方法
         * @Author FredHe
         * @Param
         * @Return {@link null}
         * @Since 2022/4/5 10:39
         */
        public default String test3(){
            System.out.println("接口默认的方法。。。。。。");
            return "hello";
        }

        public static String test4(){
            System.out.println("接口静态的方法。。。。。。");
            return "hello";
        }
    }

   static class B implements A{

        @Override
        public void test1() {

        }

        @Override
        public void test2() {

        }

       @Override
       public String test3() {
           System.out.println("B 实现类中重写了。。。。。。");
           return "ok......";
       }
   }

    static class C implements A{

        @Override
        public void test1() {

        }

        @Override
        public void test2() {

        }
    }
}
