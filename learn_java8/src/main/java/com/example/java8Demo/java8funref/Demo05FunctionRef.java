package com.example.java8Demo.java8funref;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8funref
 * @ClassName: Demo03FunctionRef
 * @Author: FredHe
 * @Date: 2022/4/9 15:49
 * @Description: 学习方法的引用
 */
public class Demo05FunctionRef {
    public static void main(String[] args) {
        Function<String,Integer> function = s -> {
            return s.length();
        };
        System.out.println(function.apply("hello"));
        //通过 方法引用 实现
        Function<String,Integer> function1 = String::length;
        System.out.println(function1.apply("world"));

        BiFunction<String,Integer,String> biFunction = String::substring;
        System.out.println(biFunction.apply("hello world",2));
    }
}
