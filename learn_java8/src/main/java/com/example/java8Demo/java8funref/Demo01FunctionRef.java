package com.example.java8Demo.java8funref;

import java.util.function.Consumer;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8funref
 * @ClassName: FunctionRef
 * @Author: FredHe
 * @Date: 2022/4/8 18:19
 * @Description: 学习方法引用
 */
public class Demo01FunctionRef {

    public static void main(String[] args) {
        printMax(a->{
            getTable(a);
        });
    }

    /**
     * 求数组中的所有元素的和
     * @param a
     */
    public static void getTable(int[] a){
        int sum = 0;
        for (int i : a) {
            sum+=i;
        }
        System.out.println(sum);
    }
    private static void printMax(Consumer<int[]> consumer){
        int[] a = {10,20,30,40,50,60};
        consumer.accept(a);
    }
}
