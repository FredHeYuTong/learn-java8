package com.example.java8Demo.java8funref;

import java.util.function.Supplier;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8funref
 * @ClassName: Demo03FunctionRef
 * @Author: FredHe
 * @Date: 2022/4/9 15:49
 * @Description: 学习方法的引用
 */
public class Demo04FunctionRef {
    public static void main(String[] args) {
        Supplier<Long> supplier = ()->{
            return System.currentTimeMillis();
        };
        System.out.println(supplier.get());
        // 通过 方法引用 来实现
        Supplier<Long> supplier1 = System::currentTimeMillis;
        System.out.println(supplier1.get());
    }
}
