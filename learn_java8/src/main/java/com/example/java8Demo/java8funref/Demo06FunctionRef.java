package com.example.java8Demo.java8funref;

import main.java.com.example.java8Demo.java8lambda.domain.Person;

import java.util.function.Supplier;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8funref
 * @ClassName: Demo03FunctionRef
 * @Author: FredHe
 * @Date: 2022/4/9 15:49
 * @Description: 学习方法的引用
 */
public class Demo06FunctionRef {
    public static void main(String[] args) {
        Supplier<Person> supplier = ()->{
            return new Person();
        };
        System.out.println(supplier.get());
        // 通过 方法引用 实现
        Supplier<Person> supplier1 = Person::new;
        System.out.println(supplier1.get());
    }
}
