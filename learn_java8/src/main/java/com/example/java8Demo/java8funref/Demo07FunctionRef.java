package com.example.java8Demo.java8funref;

import java.util.function.Function;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8funref
 * @ClassName: Demo03FunctionRef
 * @Author: FredHe
 * @Date: 2022/4/9 15:49
 * @Description: 学习方法的引用
 */
public class Demo07FunctionRef {
    public static void main(String[] args) {
        Function<Integer,String[]> function = (len)->{
            return new String[len];
        };
        String[] apply = function.apply(2);
        System.out.println("数组的长度是="+apply.length);

        Function<Integer,String[]> function1 = String[]::new;
        String[] apply1 = function1.apply(3);
        System.out.println("数组的长度是="+apply1.length);
    }
}
