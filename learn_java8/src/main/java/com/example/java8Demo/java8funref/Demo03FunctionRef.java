package com.example.java8Demo.java8funref;

import java.util.Date;
import java.util.function.Supplier;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8funref
 * @ClassName: Demo03FunctionRef
 * @Author: FredHe
 * @Date: 2022/4/9 15:49
 * @Description: 学习方法的引用
 */
public class Demo03FunctionRef {

    public static void main(String[] args) {
        Date date = new Date();
        Supplier<Long> supplier = ()->{
            return date.getTime();
        };
        System.out.println(supplier.get());
        //通过方法引用的方式来处理
        Supplier<Long> supplier1 = date::getTime;
        System.out.println(supplier1.get());
    }
}
