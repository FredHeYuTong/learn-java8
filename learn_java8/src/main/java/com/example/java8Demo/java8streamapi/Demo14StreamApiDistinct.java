package com.example.java8Demo.java8streamapi;

import main.java.com.example.java8Demo.java8lambda.domain.Person;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Dmo14StreamApiDistinct
 * @author: FredHe
 * @date: 2022/4/19 18:32
 * @description: 学习distinct  流
 */
public class Demo14StreamApiDistinct {
    public static void main(String[] args) {
        Stream.of("1", "9", "3","4","3","6","7")
                .distinct() //去掉重复数据
                .forEach(System.out::println);
        System.out.println("---------------------------------");
        Stream.of(
                new Person("张三",18,198),
                new Person("李四",22,199),
                new Person("张三",18,167)
        ).distinct()
                .forEach(System.out::println);
    }
}
