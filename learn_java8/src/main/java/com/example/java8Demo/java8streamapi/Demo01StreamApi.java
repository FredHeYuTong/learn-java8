package com.example.java8Demo.java8streamapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8streamapi
 * @ClassName: Demo01StreamApi
 * @Author: FredHe
 * @Date: 2022/4/11 17:33
 * @Description: 学习StreamApi 过滤条件
 */
public class Demo01StreamApi {

    public static void main(String[] args) {
        // 定义一个集合
        List<String> list = Arrays.asList("张三","张三丰","成龙","周星驰");
        // 1.获取所有姓张的信息
        List<String> list1 = new ArrayList<>();
        for (String s : list) {
            if (s.startsWith("张")){
                list1.add(s);
            }
        }

        // 2.获取名称长度为3的用户
        List<String> list2 = new ArrayList<>();
        for (String s : list1) {
            if (s.length() == 3){
                list2.add(s);
            }
        }

        // 3.输出所有用户信息 张姓且三个字
        for (String s: list2) {
            System.out.println(s);
        }
    }
}
