package main.java.com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo20StreamApiMapToInt
 * @author: FredHe
 * @date: 2022/4/23 17:23
 * @description: 学习mapToInt 流
 */
public class Demo20StreamApiMapToInt {
    public static void main(String[] args) {
        // Integer占用的内存比int多很多，在stream流操作中会自动拆装箱操作
        Integer arr[] = {1,2,3,4,5};
        Stream.of(arr)
                .filter(integer -> integer > 0)
                .forEach(System.out::println);
        // 为了提高程序代码的效率， 我们可以现将流中Integer数据转换为int数据，然后操作
        Integer arr2[] = {1,2,3,4,5,7,9};
        Stream.of(arr2)
                .mapToInt(Integer::intValue)
                .filter(i -> i > 4)
                .forEach(System.out::println);
    }
}
