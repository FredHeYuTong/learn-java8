package com.example.java8Demo.java8streamapi;

import java.util.Arrays;
import java.util.List;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8streamapi
 * @ClassName: Demo01StreamApi
 * @Author: FredHe
 * @Date: 2022/4/11 17:33
 * @Description: 学习StreamApi
 */
public class Demo02StreamApi {

    public static void main(String[] args) {
        // 定义一个集合
        List<String> list = Arrays.asList("张三","张三丰","成龙","周星驰");
        // 1.获取所有姓张的信息
        // 2.获取名称长度为3的用户
        // 3.输出所有用户信息 张姓且三个字
        list.stream()
                .filter(s -> s.startsWith("张"))
                .filter(s -> s.length() == 3)
                .forEach(s -> System.out.println(s));
        System.out.println("------------------------------------------------");
        list.stream()
                .filter(s -> s.startsWith("张"))
                .filter(s -> s.length() == 3)
                .forEach(System.out::println);
    }
}
