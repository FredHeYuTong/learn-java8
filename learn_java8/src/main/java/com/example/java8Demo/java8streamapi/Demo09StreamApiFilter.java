package main.java.com.example.java8Demo.java8streamapi;

import sun.applet.Main;

import java.util.Locale;
import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo09StreamApiFilter
 * @author: FredHe
 * @date: 2022/4/19 17:29
 * @description: 学习filter 拦截 流
 */
public class Demo09StreamApiFilter {
    public static void main(String[] args) {
        Stream.of("a1", "a2", "a3","bb","cc","aa","dd").filter(s -> s.contains("a"))
                .forEach(System.out::println);
    }
}
