package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo10StreamApilIMIT
 * @author: FredHe
 * @date: 2022/4/19 17:39
 * @description: 学习limit 流
 */
public class Demo10StreamApiLimit {
    public static void main(String[] args) {
        Stream.of("a1", "a2", "a3","bb","cc","aa","dd").limit(5)
                .forEach(System.out::println);
    }
}
