package com.example.java8Demo.java8streamapi;

import main.java.com.example.java8Demo.java8lambda.domain.Person;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo19StreamApiMapReduce
 * @author: FredHe
 * @date: 2022/4/20 19:16
 * @description: 学习map和reduce  流
 */
public class Demo19StreamApiMapReduce {
    public static void main(String[] args) {
        //求年龄总和
        Integer sumAge = Stream.of(
                        new Person("张三", 18, 198),
                        new Person("李四", 22, 199),
                        new Person("张三", 18, 167),
                        new Person("赵六", 19, 176),
                        new Person("张三", 33, 180)
                ).map(Person::getAge)
                .reduce(0, Integer::sum);
        System.out.println(sumAge);
        // 求出年龄最大值
        Integer maxAge = Stream.of(
                        new Person("张三", 18, 198),
                        new Person("李四", 22, 199),
                        new Person("张三", 18, 167),
                        new Person("赵六", 19, 176),
                        new Person("张三", 33, 180)
                ).map(Person::getAge)
                .reduce(0, Math::max);
        System.out.println(maxAge);
        //统计 字符 a 出现的次数
        Integer count = Stream.of("a", "b", "c", "d", "a", "c", "a")
                .map(s -> "a".equals(s) ? 1 : 0)
                .reduce(0, Integer::sum);
        System.out.println(count);
    }
}
