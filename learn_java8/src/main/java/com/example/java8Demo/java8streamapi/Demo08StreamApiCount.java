package main.java.com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo08StreamApiCount
 * @author: FredHe
 * @date: 2022/4/19 17:21
 * @description: 学习count 流
 */
public class Demo08StreamApiCount {
    public static void main(String[] args) {
        long count = Stream.of("a1", "a2", "a3").count();
        System.out.println(count);
    }
}
