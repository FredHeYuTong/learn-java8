package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo11StreamApiSkip
 * @author: FredHe
 * @date: 2022/4/19 17:45
 * @description: 学习skip  流
 */
public class Demo11StreamApiSkip {
    public static void main(String[] args) {
        Stream.of("a1", "a2", "a3","bb","cc","aa","dd").skip(3)
                .forEach(System.out::println);
    }
}
