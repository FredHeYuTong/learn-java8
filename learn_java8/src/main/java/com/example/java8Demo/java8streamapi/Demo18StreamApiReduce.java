package com.example.java8Demo.java8streamapi;

import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo18StreamApiReduce
 * @author: FredHe
 * @date: 2022/4/20 18:20
 * @description: 学习reduce 流
 */
public class Demo18StreamApiReduce {
    public static void main(String[] args) {
        Integer total = Stream.of(4, 5, 3, 9)
                // identity是默认值
                // 第一次的时候会将默认值赋给x
                // 之后每次会将上一次的操作结果赋值给x y就是每次从数据中获取的元素
                .reduce(0, (x, y) -> {
                    System.out.println("x="+x+",y="+y);
                    return x + y;
                });
        System.out.println(total);

        // 获取最大值
        Integer max = Stream.of(4, 5, 3, 9)
                .reduce(0, (x, y) -> {
                    return x > y ? x : y;
                });
        System.out.println(max);

        // 获取最小值
        Optional<Integer> reduce2 = Stream.of(4, 5, 3, 9)
                .reduce((x, y) -> {
                    return x < y ? x : y;
                });
        System.out.println(reduce2.get());
    }
}
