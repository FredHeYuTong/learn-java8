package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo06StreamApi
 * @author: FredHe
 * @date: 2022/4/16 18:00
 * @description: 学习Stream流
 */
public class Demo06StreamApi {
    public static void main(String[] args) {
        Stream<String> a1 = Stream.of("a1", "a2", "a3");
        a1.filter(s -> {
            System.out.println("------");
            return s.contains("a");})
                .forEach(System.out::println);

        System.out.println("----------------->");
    }
}
