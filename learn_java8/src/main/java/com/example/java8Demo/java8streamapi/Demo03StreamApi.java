package com.example.java8Demo.java8streamapi;

import java.util.*;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8streamapi
 * @ClassName: Demo01StreamApi
 * @Author: FredHe
 * @Date: 2022/4/11 17:33
 * @Description: 学习StreamApi
 */
public class Demo03StreamApi {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.stream();
        Set<String> set = new HashSet<>();
        set.stream();
        Vector vector = new Vector();
        vector.stream();
    }
}
