package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo12StreamApiMap
 * @author: FredHe
 * @date: 2022/4/19 17:57
 * @description: 学习map 流
 */
public class Demo12StreamApiMap {
    public static void main(String[] args) {
        Stream.of("1", "2", "3","4","5","6","7")
                //.map(s -> Integer.parseInt(s))
                .map(Integer::parseInt)
                .forEach(System.out::println);
    }
}
