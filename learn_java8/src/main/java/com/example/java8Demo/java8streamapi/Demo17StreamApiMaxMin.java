package main.java.com.example.java8Demo.java8streamapi;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo17StreamApiMaxMin
 * @author: FredHe
 * @date: 2022/4/20 18:11
 * @description: 学习max和min  流
 */
public class Demo17StreamApiMaxMin {
    public static void main(String[] args) {

        Optional<Integer> first = Stream.of("1", "9", "3", "4", "3", "6", "7")
                .map(Integer::parseInt)
                .max((o1,o2)->o1-o2);
        System.out.println(first.get());

        Optional<Integer> any = Stream.of("1", "9", "3", "4", "3", "6", "7")
                        .map(Integer::parseInt)
                        .min(((o1, o2) -> o1-o2));
        System.out.println(any.get());
    }
}
