package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo15StreamApiMatch
 * @author: FredHe
 * @date: 2022/4/20 17:49
 * @description: 学习match 流
 */
public class Demo15StreamApiMatch {
    public static void main(String[] args) {
        boolean b = Stream.of("1", "9", "3", "4", "3", "6", "7")
                .map(Integer::parseInt)
                //.allMatch(s -> s > 0)
                //.anyMatch(s -> s > 4)
                .noneMatch(s-> s > 4);
        System.out.println(b);
    }
}
