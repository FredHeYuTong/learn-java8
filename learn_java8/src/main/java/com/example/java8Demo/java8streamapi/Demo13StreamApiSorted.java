package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo3StreamApiSorted
 * @author: FredHe
 * @date: 2022/4/19 18:08
 * @description: 学习sorted  流
 */
public class Demo13StreamApiSorted {
    public static void main(String[] args) {
        Stream.of("1", "9", "3","4","22","6","7")
                .map(Integer::parseInt).distinct()
                //.sorted() //根据数据的自然顺序排序
                .sorted((o1,o2)->o2-o1) //根据比较指定排序规则
                //.sorted(Integer::compareTo)
                .forEach(System.out::println);
    }
}
