package com.example.java8Demo.java8streamapi;

import java.util.*;
import java.util.stream.Stream;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8streamapi
 * @ClassName: Demo01StreamApi
 * @Author: FredHe
 * @Date: 2022/4/11 17:33
 * @Description: 学习StreamApi
 */
public class Demo04StreamApi {
    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        Stream<String> stream1 = map.keySet().stream();
        Stream<Object> stream2 = map.values().stream();
        Stream<Map.Entry<String, Object>> stream = map.entrySet().stream();
    }
}
