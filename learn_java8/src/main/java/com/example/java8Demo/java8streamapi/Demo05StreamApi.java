package com.example.java8Demo.java8streamapi;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.java8streamapi
 * @ClassName: Demo01StreamApi
 * @Author: FredHe
 * @Date: 2022/4/11 17:33
 * @Description: 学习StreamApi
 */
public class Demo05StreamApi {
    public static void main(String[] args) {
        Stream<String> a1 = Stream.of("a1", "a2", "a3", "a4");
        String[] arr1 = {"aa","b","cc"};
        Stream<String> arr11 = Stream.of(arr1);
        Integer[] arr2 = {1,2,3,4};
        Stream<Integer> arr21 = Stream.of(arr2);
        arr21.forEach(System.out::println);
        // 注：基本数据类型的数组是不行的  会将数组数据看成一个整体
        int[] arr3 = {1,2,3,4};
        Stream.of(arr3).forEach(System.out::println);
    }
}
