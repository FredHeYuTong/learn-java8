package com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo07Streamapi
 * @author: FredHe
 * @date: 2022/4/19 17:15
 * @description: 学习foreach
 */
public class Demo07StreamApiForeach {
    public static void main(String[] args) {
       Stream.of("a1", "a2", "a3")
                .forEach(System.out::println);
    }
}
