package main.java.com.example.java8Demo.java8streamapi;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo16StreamApiFind
 * @author: FredHe
 * @date: 2022/4/20 18:04
 * @description: 学习find 流
 */
public class Demo16StreamApiFind {
    public static void main(String[] args) {

        Optional<String> first = Stream.of("1", "9", "3", "4", "3", "6", "7").findFirst();
        System.out.println(first.get());

        Optional<String> any = Stream.of("1", "9", "3", "4", "3", "6", "7").findAny();
        System.out.println(any.get());
    }
}
