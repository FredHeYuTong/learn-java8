package main.java.com.example.java8Demo.java8streamapi;

import java.util.stream.Stream;

/**
 * @projectName: learn-java8
 * @packName: com.example.java8Demo.java8streamapi
 * @className: Demo21StreamApiConcat
 * @author: FredHe
 * @date: 2022/4/23 17:55
 * @description: 学习concat 流
 */
public class Demo21StreamApiConcat {
    public static void main(String[] args) {
        Stream<String> stream1 = Stream.of("a", "b", "c");

        Stream<String> stream2 = Stream.of("x", "y", "z");
        // 通过concat方法将两个流合并成一个新的流
        Stream.concat(stream1,stream2)
                .forEach(System.out::println);
    }
}
