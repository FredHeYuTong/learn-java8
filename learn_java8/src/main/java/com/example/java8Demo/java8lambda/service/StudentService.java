package com.example.java8Demo.java8lambda.service;

/**
 * @author FredHe
 */
@FunctionalInterface
public interface StudentService {

    void show(String name,Integer age);
}
