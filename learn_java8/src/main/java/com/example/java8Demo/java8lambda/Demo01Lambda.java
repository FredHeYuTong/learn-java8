package com.example.java8Demo.java8lambda;

/**
 * PackName: com.example.java8Demo.lambda
 * ClassName: DemoLambda
 * Author:   Fred
 * Date:     2022/3/14 15:31
 * Description: 练习lambda表达式
 */
public class Demo01Lambda {

    public static void main(String[] args) {
        // 开启一个新的线程
        // new Runnable() 匿名内部类
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("新线程中执行的代码："+ Thread.currentThread().getName());
            }
        }).start();
        System.out.println("主线程中的代码："+ Thread.currentThread().getName());
        System.out.println("***********************************************************************************");
        /*new Thread(() -> {System.out.println("新线程Lambda表达式。。。。。。。。。。。："+ Thread.currentThread().getName());}).start();*/
    }
}
