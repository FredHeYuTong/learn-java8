package com.example.java8Demo.java8lambda.service;

/**
 * PackName:  com.example.java8Demo.lambda.service
 * ClassName:  UserService
 * Author:  Fred
 * Date:  2022/3/15 16:31
 * Description:  定义接口
 */

/**
 *  @FunctionalInterface
 *  这是一个标志注解，被该注解修饰的接口只能声明一个抽象方法
 */
@FunctionalInterface
public interface UserService {
    void show();
}
