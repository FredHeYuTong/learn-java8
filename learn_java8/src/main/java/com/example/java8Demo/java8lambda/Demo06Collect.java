package com.example.java8Demo.java8lambda;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @projectName: learn-java
 * @packName: com.example.java8Demo.java8lambda
 * @className: Demo06Collect
 * @author: FredHe
 * @date: 2023/2/19 17:35
 * @description: 将流中数据收集到集合中
 */
public class Demo06Collect {

    public static void main(String[] args){

        Stream<String> stream = Stream.of("aa","bb","cc");

        /* 将流中的数据收集到集合中 */
        /*List<String> collect = stream.collect(Collectors.toList());
        System.out.println("list="+collect);*/

        /*Set<String> set = stream.collect(Collectors.toSet());
        System.out.println("set="+set);*/

        /* 收集到指定的集合中ArrayList */
        /*ArrayList<String> arrayList = stream.collect(Collectors.toCollection(ArrayList::new));
        System.out.println("arrayList="+arrayList);*/

        /* 收集到指定的集合中HashSet */
        /*HashSet<String> hashSet = stream.collect(Collectors.toCollection(HashSet::new));
        System.out.println("hashSet="+hashSet);*/

        /* 转object数组不方便其他操作 */
        /*Object[] objects = stream.toArray();
        for (Object object : objects) {
            System.out.println("object="+object);
        }*/

        /* 转String数组可以进行其他操作 */
        String[] strings = stream.toArray(String[]::new);
        for (String string : strings) {
            System.out.println("string="+string+", 长度="+string.length());
        }

    }
}
