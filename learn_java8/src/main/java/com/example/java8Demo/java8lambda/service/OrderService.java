package com.example.java8Demo.java8lambda.service;

/**
 * @author FredHe
 */
@FunctionalInterface
public interface OrderService {

    Integer show(String name);
}
