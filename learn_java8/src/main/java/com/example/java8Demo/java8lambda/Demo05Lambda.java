package com.example.java8Demo.java8lambda;

import com.example.java8Demo.java8lambda.service.OrderService;
import com.example.java8Demo.java8lambda.service.StudentService;

/**
 * @ProjectName: learn-java8
 * @PackName: com.example.java8Demo.lambda
 * @ClassName: Demo05Lambda
 * @Author: FredHe
 * @Date: 2022/3/28 21:39
 * @Description: 学习
 */
public class Demo05Lambda {

    public static void main(String[] args) {
        goStudent((String name,Integer age) ->{
            System.out.println(name+"======="+age);
        });

    }

    public static void goStudent(StudentService studentService){
        studentService.show("张三",22);
    }

    public static void goOrde(OrderService orderService){
        orderService.show("李四");
    }
}
