package com.example.java8Demo.java8lambda;

import com.example.java8Demo.java8lambda.service.UserService;

/**
 * PackName:  com.example.java8Demo.lambda
 * ClassName:  Demo03Lambda
 * Author:  Fred
 * Date:  2022/3/15 16:23
 * Description:  学习Lambda表达式 练习1 无参无返回值的Lambda
 */
public class Demo03Lambda {

    public static void main(String[] args) {
        goShow(new UserService() {
            @Override
            public void show() {
                System.out.println("show 方法执行了。。。。。。。。。。。。。。。。");
            }
        });
        System.out.println("----------------------------------------");
        goShow(()->{
            System.out.println("Lambda show 方法执行了。。。。。。。。。。。。。。。。");
        });
    }

    public static void goShow(UserService userService){
        userService.show();
    }

}
