package com.example.java8Demo.java8lambda;

import main.java.com.example.java8Demo.java8lambda.domain.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * PackName:  com.example.java8Demo.lambda
 * ClassName:  Demo04Lambda
 * Author:  Fred
 * Date:  2022/3/15 16:40
 * Description:  学习Lambda表达式 练习2  有参且有返回值的Lambda
 */
public class Demo04Lambda {

    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        list.add(new Person("周杰伦",33,175));
        list.add(new Person("刘德华",43,185));
        list.add(new Person("周星驰",38,177));
        list.add(new Person("郭富城",23,170));

        /*Collections.sort(list, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge()-o2.getAge();
            }
        });

        for (Person person:list) {
            System.out.println(person);
        }*/
        System.out.println("---------------------------");

        Collections.sort(list,(Person o1,Person o2) ->{
            return o1.getAge() - o2.getAge();
        });
        for (Person person : list) {
            System.out.println(person);
        }
    }
}
