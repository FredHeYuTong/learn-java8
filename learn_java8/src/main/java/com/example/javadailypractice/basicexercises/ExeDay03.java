package com.example.javadailypractice.basicexercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @projectName: learn-java8
 * @packName: com.example.javadailypractice.basicexercises
 * @className: ExeDay03
 * @author: FredHe
 * @date: 2022/8/19 14:47
 * @description: 排序练习
 * 请关闭中文输入法，用英文的字母和标点符号。
 * // 如果你想运行系统测试用例，请点击【执行代码】按钮，如果你想提交作答结果，请点击【提交】按钮，
 * // 注意：除答案外，请不要打印其他任何多余的字符，以免影响结果验证
 * // 本OJ系统是基于 OxCoder 技术开发，网址：www.oxcoder.com
 */
public class ExeDay03 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str_0 = scan.nextLine().trim();
        int n = Integer.parseInt(str_0);


        String str_1 = scan.nextLine();
        String[] line_list_1 = str_1.trim().split(" ");
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i = 0; i < line_list_1.length; i++) {
            arr.add(Integer.parseInt(line_list_1[i]));
        }


        scan.close();

        ArrayList<Integer> result = solution(n, arr);


        for (int i = 0; i < result.size(); i++) {
            System.out.print(result.get(i) + " ");
        }


    }

    public static ArrayList<Integer> solution(int n, ArrayList<Integer> arr) {
        ArrayList<Integer> result = new ArrayList<>();

        // TODO: 请在此编写代码
        if(n != arr.size()){
            result = null;
        }else {
            result = (ArrayList<Integer>) arr.stream().sorted((o1, o2) -> o1 - o2).collect(Collectors.toList());
        }

        return result;
    }
}
