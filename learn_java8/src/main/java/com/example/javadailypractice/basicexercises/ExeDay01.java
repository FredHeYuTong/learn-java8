package com.example.javadailypractice.basicexercises;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
  * @ProjectName: learn-java8
  * @PackName:  dailypractice.basicexercises
  * @ClassName:  ExeDay01
  * @Author:  FredHe
  * @Date:  2022/4/1 21:39
  * @Description:  java基础练习第一天
  */
public class ExeDay01 {

    public static void main(String[] args) {

        Date date = new Date();
        System.out.println(date);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        System.out.println(format.format(date));

    }
}
