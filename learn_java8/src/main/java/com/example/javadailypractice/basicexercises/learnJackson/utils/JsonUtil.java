package com.example.javadailypractice.basicexercises.learnJackson.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @projectName: learn-java8
 * @packName: com.example.javadailypractice.basicexercises.learnJackson
 * @className: JsonUtil
 * @author: FredHe
 * @date: 2022/7/7 9:37
 * @description: JSON转换类
 */
public final class JsonUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();
    
    private JsonUtil() {

    }

    /**
     * @methodName
     * @description Serialize any Java value as a String.
     * @author FredHe
     * @param
     * @param object
     * @return
     * @return null
     * @since 2022/7/7 9:53
     */
    public static String generate(Object object) throws JsonProcessingException{

        return objectMapper.writeValueAsString(object);
    }

    /**
     * @methodName parse
     * @description  Deserialize JSON content from given JSON content String.
     *               反序列化 将JSON转换为string
     * @author FredHe
     * @param 
     * @param content
     * @param valueType
     * @return 
     * @return T
     * @since 2022/7/7 10:13
     */
    public static <T> T parse(String content,Class<T> valueType) throws IOException{

        return objectMapper.readValue(content,valueType);
    }

}
