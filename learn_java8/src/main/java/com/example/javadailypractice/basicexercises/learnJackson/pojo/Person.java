package com.example.javadailypractice.basicexercises.learnJackson.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @projectName: learn-java8
 * @packName: com.example.javadailypractice.basicexercises.learnJackson.pojo
 * @className: Person
 * @author: FredHe
 * @date: 2022/7/7 10:59
 * @description: 实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"firstName","lastName"})
@JsonIgnoreProperties({"id"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person {


    private Integer id;

    private String firstName;

    private String lastName;
}
