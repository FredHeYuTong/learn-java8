package com.example.javadailypractice.basicexercises;

import java.util.Scanner;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @projectName: learn-java8
 * @packName: com.example.javadailypractice.basicexercises
 * @className: ExeDay02
 * @author: FredHe
 * @date: 2022/6/21 16:13
 * @description: 线程练习
 */
public class ExeDay02 {

    public static void main(String[] args) throws InterruptedException {

        BlockingDeque<String> blockingDeque = new LinkedBlockingDeque<>();

        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        blockingDeque.put(str);

        while ((str = blockingDeque.take())!=null){
            System.out.println(str);
            String s = scanner.nextLine();
            blockingDeque.put(s);
        }
    }
}
