package com.example.javadailypractice.basicexercises.learnJackson.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @projectName: learn-java8
 * @packName: com.example.javadailypractice.basicexercises.learnJackson.pojo
 * @className: Hero
 * @author: FredHe
 * @date: 2022/7/7 10:15
 * @description: 实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hero {

    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

}
