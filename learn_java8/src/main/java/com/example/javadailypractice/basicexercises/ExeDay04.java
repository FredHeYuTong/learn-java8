package com.example.javadailypractice.basicexercises;

/**
 * @projectName: learn-java
 * @packName: com.example.javadailypractice.basicexercises.learnJackson
 * @className: ExeDay04
 * @author: FredHe
 * @date: 2022/12/20 9:13
 * @description: 联系04
 */
public class ExeDay04 {

    public static void main(String[] args) {
        Double l = 9.423523;

        System.out.println(Math.round(l * 1000) / 1000.000);
    }
}
