package com.example.javadailypractice.basicexercises.learnJackson;

import com.example.javadailypractice.basicexercises.learnJackson.pojo.Person;
import com.example.javadailypractice.basicexercises.learnJackson.utils.JsonUtil;

import java.io.IOException;

/**
 * @projectName: learn-java8
 * @packName: com.example.javadailypractice.basicexercises.learnJackson
 * @className: ExeJackson
 * @author: FredHe
 * @date: 2022/7/7 9:10
 * @description: 练习Jackson解析JSON
 */
public class ExeJackson {

    public static void main(String[] args) throws IOException {

        // System.out.println(JsonUtil.generate(new Hero("Jason", new Date())));

        System.out.println(JsonUtil.generate((new Person(1, "Jason", "he"))));

        // System.out.println(JsonUtil.generate(new Person(1, "Jason",null)));

        System.out.println(JsonUtil.parse("{\"firstName\":\"Jason\",\"lastName\":\"he\"}", Person.class).getFirstName());
    }
}
