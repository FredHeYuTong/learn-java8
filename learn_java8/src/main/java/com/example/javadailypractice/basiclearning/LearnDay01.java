package com.example.javadailypractice.basiclearning;

/**
 * @ProjectName: learn-java8
 * @PackName: dailypractice.basiclearning
 * @ClassName: LearnDay01
 * @Author: FredHe
 * @Date: 2022/4/1 21:37
 * @Description: java基础学习第一天
 * 知识点：1.静态成员属于类,不需要生成对象就存在了.而非静态需要生成对象才产生.所以静态成员不能直接访问非静态.
 */
public class LearnDay01 {

    public static void method(String cell){

        String hello = "hell" + cell;
        System.out.println(hello);
    }

    public static void main(String[] args) {
        method("xiaoliu");
    }
}
