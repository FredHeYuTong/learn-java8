package com.example.javadailypractice.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test02 {




    public static double getDistanceOfTwoDate(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
    }
    

    public static void main(String[] args) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            Date date1 = format.parse("2024-10-16 20:00:37");
            double distance = getDistanceOfTwoDate(date1, date);
            System.out.println(distance);
            if (distance >= 1.0){
                System.out.println("大于一天");
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
