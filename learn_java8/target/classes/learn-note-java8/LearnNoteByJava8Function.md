## 函数式接口

### 1.函数式接口的由来

        我们知道使用Lambda表达式的前提是需要有函数式接口，而Lambda表达式使用的是不关心接口名，抽象方法名。
    只关心抽象方法的参数列表和返回值类型。因此为了让我们使用Lambda表达式更加方便，在jdk中提供了大量常用的函数式接口。

### 2.函数式接口介绍
    
    在jdk中帮我们提供的函数式接口，主要是在java.util.function包中。
-
    2.1Supplier
        无参有返回值的接口，对于Lambda表达式需要提供一个返回数据的类型。用来生产数据的。
```java
@FunctionalInterface
public interface Supplier<T> {

    /**
     * Gets a result.
     *
     * @return a result
     */
    T get();
}
```
    例：
```java
public class DemoLearnSupplier {
    public static void main(String[] args) {
        fun1(()->{
            int arr[] = {22,33,55,66,44,99,10};
            //计算数组的最大值
            Arrays.sort(arr);
            return arr[arr.length-1];
        });
    }

    private static void fun1(Supplier<Integer> supplier){
        //get() 是一个无参有返回值的 抽象方法
        Integer max = supplier.get();
        System.out.println("max="+max);
    }
}
```
- 
    2.2Consumer
        
    有参有返回值的接口，是用来消费的，使用的时候需要指定一个泛型来定义参数类型。
```java
@FunctionalInterface
public interface Consumer<T> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void accept(T t);
}
```
    例：将数据转换为小写
```java
public class DemoLearnConsumer {
    public static void main(String[] args) {
        test(msg ->
                //有参 无返回值 使用省略写法
            System.out.println(msg+"->转换为小写"+msg.toLowerCase(Locale.ROOT))
        );
    }
    private static void test(Consumer<String> consumer){
        consumer.accept("Hello World");
    }
}
```
    默认方法：andThen  
       如果一个方法的参数和返回值全部是Consumer，那么就可以实现效果，消费一个数据的时候，首先做一个操作， 
    然后再做一个操作，实现组合，而这个方法就是Consumer接口中的default方法andThen方法
```java
public class DemoLearnConsumer {
    public static void main(String[] args) {
        test2(msg1 -> {
            System.out.println(msg1 + "->转换为小写" + msg1.toLowerCase(Locale.ROOT));
        }, msg2 -> {
            System.out.println(msg2 + "->转换为大写" + msg2.toUpperCase(Locale.ROOT));
        });
    }
    private static void test2(Consumer<String> c1, Consumer<String> c2) {
        String str = "Hello World";
        c1.andThen(c2).accept(str);
    }
}
```
- 
    2.3Function

    有参有返回值的接口，Function接口是根据一个类型的数据得到另一个类型的数据，前者称为前置条件，后者称为 
    后置条件
```java
@FunctionalInterface
public interface Function<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t);
}
```
    使用方法：传递一个字符串返回一个数字
```java
public class DemoLearnFunction {

    public static void main(String[] args) {
        test(msg->{
            return Integer.parseInt(msg);
        });
    }

    public static void test(Function<String,Integer> function){
        Integer apply = function.apply("666");
        System.out.println("apply = "+ apply);
    }
}
```
    默认方法：andThen，也是用来进行组合操作
```java
public class DemoLearnFunctionAndThen {

    public static void main(String[] args) {
        test(msg->{
            return Integer.parseInt(msg);
        },msg2->{
            return msg2*10;
        });
    }

    public static void test(Function<String,Integer> function,Function<Integer,Integer> function2){
        Integer apply = function.apply("666");
        Integer apply1 = function2.apply(apply);
        System.out.println("apply1 = "+ apply1);
    }
}
```
    默认的compose方法的作用顺序和andThen方法刚好相反 
    而静态方法identity则是，输入什么参数就返回什么参数
-
    2.4Predicate
        有参有返回值 返回值是boolean类型的接口
```java
@FunctionalInterface
public interface Predicate<T> {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param t the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    boolean test(T t);
}
```
    例：
```java
public class DemoLearnPredicate {

    public static void main(String[] args) {
        test(msg->{
            return msg != null;
        });

        test2(msg->{
            return msg.length()>100;
        },"chuidhiughiuh");
    }

    public static void test(Predicate<String> predicate){
        boolean hello = predicate.test("hello");
        System.out.println("hello="+hello);
    }

    private static void test2(Predicate<String> predicate,String msg){
        boolean test = predicate.test(msg);
        System.out.println("test="+test);
    }
}
```
    在predicate中的默认方法提供了逻辑关系操作and or negate isEquals方法
```java
public class DemoLearnPredicateDefault {

    public static void main(String[] args) {

        test(msg1->{
            return msg1.contains("H");
        },msg2->{
            return msg2.contains("W");
        });
    }
    private static void test(Predicate<String> p1,Predicate<String> p2){
        boolean test = p1.test("Hello");
        boolean test2 = p2.test("World");
        //test 包含h test2 包含w
        System.out.println("test="+test);
        System.out.println("test2="+test2);
        //test 同时包含h test2 同时包含w
        boolean hello = p1.and(p2).test("Hello");
        //test 包含h 或者 test2 包含w
        boolean hello1 = p1.or(p2).test("Hello");
        //test 不包含h
        boolean hello2 = p1.negate().test("Hello");

        System.out.println(hello);  //false
        System.out.println(hello1); //true
        System.out.println(hello2); //false
    }
}
```
