
## 方法引用

### 1.为什么要用方法引用

- 
    1.1  Lambda表达式冗余

  在使用Lambda表达式时，也会出现代码冗余的情况，比如：用Lambda表达式求一个数组的和
```java
public class Demo01FunctionRef {
    public static void main(String[] args) {
        printMax(a->{
            getTable(a);
        });
    }
    /**
     * 求数组中的所有元素的和
     * @param a
     */
    public static void getTable(int[] a){
        int sum = 0;
        for (int i : a) {
            sum+=i;
        }
        System.out.println(sum);
    }
    private static void printMax(Consumer<int[]> consumer){
        int[] a = {10,20,30,40,50,60};
        consumer.accept(a);
    }
}
```
- 
  1.2   解决方案 

  因为在Lambda表达式中，要执行的代码和我们另一个方法中的代码是一样的，这时就没有必要重写一份逻辑了，就可以“引用”
重复代码
```java
public class Demo02FunctionRef {
    public static void main(String[] args) {
        //::方法的引用
        printMax(Demo02FunctionRef::getTable);
    }

    /**
     * 求数组中的所有元素的和
     * @param a
     */
    public static void getTable(int[] a){
        int sum = 0;
        for (int i : a) {
            sum+=i;
        }
        System.out.println(sum);
    }
    private static void printMax(Consumer<int[]> consumer){
        int[] a = {10,20,30,40,50,60};
        consumer.accept(a);
    }
}
```
### 2.方法引用的格式

    符号表示： ::
    符号说明：双冒号为方法引用运算符，而他所在的表达式被称为方法的引用
    应用场景：如果Lambda表达式所要实现的方案，已经有其他方法存在相同的方案，那么则可以使用方法的引用
    常见的引用方式：
        方法的引用在jdk8中使用是相当灵活的，有以下几种形式：
          1.instanceName::methodName 对象::方法名
          2.ClassName::staticMethodName 类名::静态方法
          3.ClassName::methodName 类名::普通方法
          4.ClassName::new 类名::new调用的构造器
          5.TypeName[]::new String[]::new 调用数组的构造器

- 2.1 对象名::方法名 

   最常见的一种方法。如果一个类中已经存在了一个成员方法，则可以通过对象名引用成员方法
```java
public class Demo03FunctionRef {
    public static void main(String[] args) {
        Date date = new Date();
        Supplier<Long> supplier = ()->{
            return date.getTime();
        };
        System.out.println(supplier.get());
        //通过方法引用的方式来处理
        Supplier<Long> supplier1 = date::getTime;
        System.out.println(supplier1.get());
    }
}
```
      方法引用的注意事项：
          1.被引用的方法，参数要和接口中的抽象方法的参数一样
          2.当借口抽象方法有返回值是，被引用的方法也必须有返回值
- 2.2 类名::静态方法 
```java
public class Demo04FunctionRef {
    public static void main(String[] args) {
        Supplier<Long> supplier = ()->{
            return System.currentTimeMillis();
        };
        System.out.println(supplier.get());
        // 通过 方法引用 来实现
        Supplier<Long> supplier1 = System::currentTimeMillis;
        System.out.println(supplier1.get());
    }
}
```
- 2.3 类名::普通方法  
      java面向对象，类名只能调用静态方法，类名引用实例方法是有前提的，实际上是拿第一个参数作为方法的 
调用者
```java
public class Demo05FunctionRef {
    public static void main(String[] args) {
        Function<String,Integer> function = s -> {
            return s.length();
        };
        System.out.println(function.apply("hello"));
        //通过 方法引用 实现
        Function<String,Integer> function1 = String::length;
        System.out.println(function1.apply("world"));

        BiFunction<String,Integer,String> biFunction = String::substring;
        System.out.println(biFunction.apply("hello world",2));
    }
}
```
- 2.4 类名::构造器 
       
 由于构造器的名称和类名完全一致，所以构造器引用使用::new的格式
```java
public class Demo06FunctionRef {
    public static void main(String[] args) {
        Supplier<Person> supplier = ()->{
            return new Person();
        };
        System.out.println(supplier.get());
        // 通过 方法引用 实现
        Supplier<Person> supplier1 = Person::new;
        System.out.println(supplier1.get());
    }
}
```
- 2.5 数组::构造器 

 如何构造数组 

 ```java
public class Demo07FunctionRef {
    public static void main(String[] args) {
        Function<Integer,String[]> function = (len)->{
            return new String[len];
        };
        String[] apply = function.apply(2);
        System.out.println("数组的长度是="+apply.length);

        Function<Integer,String[]> function1 = String[]::new;
        String[] apply1 = function1.apply(3);
        System.out.println("数组的长度是="+apply1.length);
    }
}
```

    小结：方法引用是对Lambda表达式符合特定情况下的一种缩写方式，它使得我们的Lambda表达式更加精简， 
    也可以理解为Lambda表达式的缩写形式，不过要注意的是方法引用只能引用已经存在的方法。