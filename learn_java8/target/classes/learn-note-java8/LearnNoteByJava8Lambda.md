## 二. Lambda表达式

### 1.需求分析

    创建一个新的线程，指定线程要执行的任务

~~~java
        public static void main(String[]args){
        // 开启一个新的线程
        // new Runnable() 匿名内部类
        new Thread(new Runnable(){
        @Override
        public void run(){
        System.out.println("新线程中执行的代码："+Thread.currentThread().getName());
        }
        }).start();
        System.out.println("主线程中的代码："+Thread.currentThread().getName());
        }
~~~    

### 2.代码分析：

        1.Thread类需要一个Runnable接口作为参数，其中的抽象方法run方法是用来指定线程任务内容的核心
        2.为了指定run方法体，不得不需要Runnable的实现类
        3.为了省去定义一个Runnable的实现类，不得不使用匿名内部类
        4.必须覆盖重写抽象的run方法，所有的方法名称，方法参数，方法返回值不得不都重写一遍，而且不能出错
        5.实际上，我们只在乎方法体上的代码

### 3.Lambda表达式初体验

        Lambda表达式是一个匿名函数，可以理解为一段可以传递的代码
        new Thread(() -> {System.out.println("新线程Lambda表达式。。。。。。。。。。。："+ Thread.currentThread().getName());}).start();
        Lambda表达式的优点：简化了匿名内部类的使用，语法更加简单。
        匿名内部类语法冗余，体验了Lambda表达式后，发现Lambda表达式是简化匿名内部类的一种方式。

### 4.Lambda的语法规则

        省去了面向对象的条条框框，标准格式由3个部分组成：

~~~
         (参数类型 参数名称) -> {
                    代码体;
                }
~~~

        格式说明：
        (参数类型 参数名称)：参数列表
        {代码体;}：方法体
        ->：箭头，分割参数列表和方法体

### 5.@FunctionalInterface注解

~~~java
/**
 * @FunctionalInterface
 * 这是一个标志注解，被该注解修饰的接口只能声明一个抽象方法
 */
@FunctionalInterface
public interface UserService {
    void show();
}
~~~

### 6.Lambda表达式的原理

        匿名内部类的本质是在编译时生成一个Class文件。
        Lambda表达式在程序运行的时候会形成一个类。
            1.在类中新增了一个方法，这个方法的方法体就是Lambda表达式中的代码
            2.还会形成一个匿名内部类，实现接口，重写实现方法
            3.在借口中重写方法会调用新生成的方法

### 7.Lambda表达式的省略写法 简写

        在表达式的标准写法基础上，可以使用省略写法的规则为：
            1.小括号内的参数类型可以省略
            2.如果小括号内有且仅有一个参数，则小括号可以省略
            3.如果大括号内有且仅有一个语句，可以同时省略大括号，return关键字及语句分号

### 8.Lambda表达式的使用前提

        Lambda表达式的语法是非常简洁的，但是Lambda表达式不是随便使用的，使用时有几个条件要特别注意
            1.方法的参数或者局部变量类型必须为接口才能使用  
            2.接口中有且仅有一个抽象方法@FunctionalInterface

### 9.Lambda和匿名内部类的对比
        
        1.所需类型不一样
            a.匿名内部类的类型可以是类、抽象类、接口
            b.Lambda表达式需要的类型必须是接口用
        2.实现方法数量不一样
            a.匿名内部类所需的接口中抽象方法的数量是随意的
            b.Lambda表达式所需的接口中只能有一个抽象方法
        3.实现原理不一样
            a.匿名内部类是在变以后形成一个class文件
            b.Lamb表达式是在程序运行的时候动态生成class
        


        