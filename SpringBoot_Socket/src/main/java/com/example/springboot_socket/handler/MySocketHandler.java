package com.example.springboot_socket.handler;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.example.springboot_socket.util.MySocketUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @projectName: learn-java
 * @packName: com.example.springboot_socket.handler
 * @className: MySocketHandler
 * @author: FredHe
 * @date: 2022/12/3 15:37
 * @description: socket拦截器
 */
@Slf4j
public class MySocketHandler {

    @Autowired
    private SocketIOServer socketIoServer;

    @PostConstruct
    private void start(){
        try {
            socketIoServer.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @PreDestroy
    private void destroy(){
        try {
            socketIoServer.stop();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @OnConnect
    public void connect(SocketIOClient client) {
        String userFlag = client.getHandshakeData().getSingleUrlParam("userFlag");
        MySocketUtil.connectMap.put(userFlag, client);
        log.info("客户端userFlag: "+ userFlag+ "已连接");
    }
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
        String userFlag = client.getHandshakeData().getSingleUrlParam("userFlag");
        log.info("客户端userFlag:" + userFlag + "断开连接");
        MySocketUtil.connectMap.remove(userFlag, client);
    }
}
