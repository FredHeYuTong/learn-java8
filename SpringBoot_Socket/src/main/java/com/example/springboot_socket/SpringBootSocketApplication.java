package com.example.springboot_socket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author FredHe
 */
@SpringBootApplication
public class SpringBootSocketApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSocketApplication.class, args);
    }

}
