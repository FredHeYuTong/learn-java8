package com.example.springboot_socket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

/**
 * @projectName: learn-java
 * @packName: com.example.springboot_socket.dto
 * @className: MyMessage
 * @author: FredHe
 * @date: 2022/12/3 15:37
 * @description: 消息实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyMessage {
    private String type;

    private String content;

    private String from;

    private String to;

    private String channel;

}
