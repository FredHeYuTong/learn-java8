package com.example.springboot_socket.controller;

import com.example.springboot_socket.dto.MyMessage;
import com.example.springboot_socket.util.MySocketUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @projectName: learn-java
 * @packName: com.example.springboot_socket.controller
 * @className: MySocketController
 * @author: FredHe
 * @date: 2022/12/3 15:36
 * @description: 接口类
 */
@RestController
public class MySocketController {

    public final static String SEND_TYPE_ALL = "ALL";
    public final static String SEND_TYPE_ALONE = "ALONE";

    @Autowired
    private MySocketUtil mySocketUtil;

    @PostMapping("/testSendMsg")
    public String testSendMsg(@RequestBody MyMessage myMessage){
        Map<String, Object> map = new HashMap<>();
        map.put("msg",myMessage.getContent());

        //群发
        if (SEND_TYPE_ALL.equals(myMessage.getType())){
            mySocketUtil.sendToAll( map,myMessage.getChannel());
            return "success";
        }
        //指定单人
        if (SEND_TYPE_ALONE.equals(myMessage.getType())){
            mySocketUtil.sendToOne(myMessage.getTo(), map, myMessage.getChannel());
            return "success";
        }

        return "fail";
    }
}
